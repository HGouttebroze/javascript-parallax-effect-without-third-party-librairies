# JavaScript parallax effect without third party librairies 

GOALS: avoid having to load a library with a large weight to create a parallax effect in JS. The objective is to using code genericity to make different elements of the page scroll faster or slower than the scroll to give a perspective & depth effect